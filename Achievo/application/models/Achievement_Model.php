<?php 

class Achievement_Model extends CI_Model {

private $idUtilisateur;
const REQ_OK = 0;
const REQ_LOGIN = 'Les informations de connections sont fausse !';

	public function __construct(){
		$this->load->database();
	}

	public function setIdUtilisateur ($idUtilisateur) {
		$this->idUtilisateur = $idUtilisateur;
	}

	public function getIdUtilisateur ($entreprise) {
		$select = "SELECT id FROM utilisateur WHERE entreprise = ?";
		$query = $this->db->query($select, array($entreprise));

		return $query->row()->id;
	}

	public function register($username,$password){
		$data = array(
			"entreprise"   => $username,
			"mdp"   => hash("sha512", "ok@/" . $password)
		);
		return $this->db->insert("utilisateur",$data);
	}

	public function checkUser($username,$password) {
		$select = "SELECT id FROM utilisateur WHERE entreprise = ? AND mdp = ?";		
		$query = $this->db->query($select, array($username, hash("sha512", "ok@/" . $password)));
		if (!$query) {
			return REQ_LOGIN;
		} else {
			return $query->row();
		}
		

	}

	public function getAchievement() {
		$select ='SELECT * FROM succes WHERE idutilisateur = ? ORDER BY id ASC';
		$query = $this->db->query($select, array($this->idUtilisateur));

		return $query->result_array();
	}

	public function createAchievement ($titre, $intitule, $objectif) {
		$data = array(
			"titre"   => $titre,
			"intitule"   => $intitule,
			"objectif"   => $objectif,
			"progression"   => 0,
			"idutilisateur"   => $this->idUtilisateur
		);
		return $this->db->insert("succes",$data);
	}

	public function deleteAchievement ($id) {
		$delete = 'DELETE FROM succes WHERE id = ? AND idutilisateur = ?';
		$query = $this->db->query($delete, array($id, $this->idUtilisateur));
		return 0;
	}

	public function vue ($id) {
		$select ='SELECT * FROM succes WHERE id = ? AND idutilisateur = ?';
		$query = $this->db->query($select, array($id, $this->idUtilisateur));

		return $query->row_array();
	}

	public function updateAchievement ($newProgression, $id) {
		$update = 'UPDATE succes SET progression = ? WHERE id = ? AND idutilisateur = ?';
		$query = $this->db->query($update, array($newProgression, $id, $this->idUtilisateur));
		return 0;
	}
}