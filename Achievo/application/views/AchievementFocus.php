<!DOCTYPE html>
<html>
<head>
	<title>Achievo | Vue</title>
</head>
<body>
	<article style="background-color: #CCC6C8; text-align: center; border: 2px solid black; width: 600px; height: 150px; margin: 0% 25% 2% 33%;  padding: 1% 1% 1% 1%;">
		<h2><?= $data['titre']?></h2>
		<p><?= $data['intitule']?></p>
		<form method="post" action="http://quentinp.dijon.codeur.online/Achievo/update/<?=$data['id']?>">
			<input type="number" min="0" max="<?= $data['objectif']?>" value="<?= $data['progression']?>" name="progression"/> 
			<input type="number" value="<?= $data['objectif']?>" disabled="disabled"/> 
			<input type="submit" value="Mettre à jour"/>
		</form>
		<form method="post" action="http://quentinp.dijon.codeur.online/Achievo/affiche">
    		<button type="submit">Page principale</button>
		</form>
	</article>
</body>
</html>


