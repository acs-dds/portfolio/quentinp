<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Achievement_Controller extends CI_Controller{
	public function __construct(){
		parent::__construct();

		$this->load->model('Achievement_Model');
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->library('form_validation');
		// Si la session est pr�sente, je lock le model pour qu'il n'utilise que les donn�es li� a cette idUtilisateur
		if ($this->session->userdata('current')) {
			$this->Achievement_Model->setIdUtilisateur($this->Achievement_Model->getIdUtilisateur($this->session->userdata('current')));
		}
		$this->output->enable_profiler(true);
	}

	public function index($type = ''){
		if (empty($this->session->userdata('current'))) {
			$this->load->view('login');
		}
		if ($type = 'mauvaisinfo') {
			$erors = "Informations de connection incorect";
			$this->load->view('login', $erors);
		}
		else {
			$this->load->view('Achievement', ['data' => $this->Achievement_Model->getAchievement()]);
		}
		
	}

	public function deco () {
		$this->session->unset_userdata('current');
		redirect('index');
	}

	public function check() {
		$user = $this->input->post('user');
		$pass = $this->input->post('psw');
		$cu = $this->Achievement_Model->checkUser($user,$pass);
		switch ($cu) {
			case Achievement_Model::REQ_LOGIN:

				redirect('Achievement_Controller/index/mauvaisinfo');
				break;
			
			default:
				$data = array('current'  => $this->input->post("user"));
				$this->session->set_userdata($data);
				redirect('affiche');
				break;
		}
	}

	public function affiche() {
		$this->load->view('Achievement', ['data' => $this->Achievement_Model->getAchievement()]);
	}

	public function view ($id) {
		if (empty($this->session->userdata('current'))) {
			redirect('index');
		}
		if ($Achievement = $this->Achievement_Model->vue($id)) {
			$this->load->view('AchievementFocus', ['data' => $Achievement]);
		}
		else {
			redirect('index');
		}
	}

	public function maj ($id) {
		if (empty($this->session->userdata('current'))) {
			redirect('index');
		}
		$newProgression = $this->input->post('progression');
		$idUtilisateur = $this->Achievement_Model->getIdUtilisateur($this->session->userdata('current'));
		$this->Achievement_Model->updateAchievement($newProgression, $id, $idUtilisateur);
		redirect('vue/'.$id);
	}

	public function inscription (){
		$this->form_validation->set_rules("pseudo","Pseudo","trim|required|min_length[5]|max_length[12]");
		$this->form_validation->set_rules("pass","Pass","trim|required|min_length[5]");
		$this->form_validation->set_rules("passconfirm","Passconfirm","trim|required|matches[pass]");

		if($this->form_validation->run() === false){
			redirect('index');
		}
		else{
			$pseudo = $this->input->post('pseudo');
			$pass = $this->input->post('pass');
			$passconfirm = $this->input->post('passconfirm');
			$verif = $this->Achievement_Model->register($pseudo,$pass);
			if(!$verif){
				redirect('index');
			} else {
				redirect('affiche');
			}
		}
	}

	public function create () {
		if (empty($this->session->userdata('current'))) {
			redirect('index');
		}
		$titre = $this->input->post('titre');
		$intitule = $this->input->post('intitule');
		$objectif = $this->input->post('objectif');
		$verif = $this->Achievement_Model->createAchievement($titre,$intitule,$objectif);
		redirect('affiche');
	}

	public function delete ($id) {
		if (empty($this->session->userdata('current'))) {
			redirect('index');
		}
		if($this->Achievement_Model->deleteAchievement($id) == 0){
			redirect('affiche');
		}

	}
}