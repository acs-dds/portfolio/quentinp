/*CREATE DATABASE ??????;*/

CREATE TABLE succes (
    id integer NOT NULL,
    titre character varying(200) NOT NULL,
    intitule character varying(600) NOT NULL,
    objectif integer NOT NULL,
    progression integer NOT NULL,
    idutilisateur integer NOT NULL,
    CONSTRAINT succes_check CHECK ((progression <= objectif)),
    CONSTRAINT succes_objectif_check CHECK ((objectif > 0))
);


CREATE SEQUENCE succes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE succes_id_seq OWNED BY succes.id;

CREATE TABLE utilisateur (
    id integer NOT NULL,
    entreprise character varying(70) NOT NULL,
    mdp character(128) NOT NULL
);

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1


CREATE VIEW vuesucces AS
 SELECT succes.id,
    succes.titre,
    succes.intitule,
    succes.objectif,
    succes.progression,
    succes.idutilisateur,
    (succes.progression = succes.objectif) AS fini
   FROM succes
UNION
 SELECT 0 AS id,
    'C''est qui le patron ?'::character varying AS titre,
    'Vous avez atteint tous les objectifs que vous vous êtes fixés ! N''hésitez pas à en définir de nouveaux ;-)'::character varying AS intitule,
    tous.c AS objectif,
    COALESCE(finis.c, (0)::bigint) AS progression,
    tous.i AS idutilisateur,
    COALESCE((tous.c = finis.c), false) AS fini
   FROM (( SELECT count(*) AS c,
            succes.idutilisateur AS i
           FROM succes
          GROUP BY succes.idutilisateur) tous
     LEFT JOIN ( SELECT count(*) AS c,
            succes.idutilisateur AS i
           FROM succes
          WHERE (succes.progression = succes.objectif)
          GROUP BY succes.idutilisateur) finis ON ((tous.i = finis.i)));


ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_pkey PRIMARY KEY (id);

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_titre_unique UNIQUE (titre, idutilisateur);

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_entreprise_key UNIQUE (entreprise);

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_idutilisateur_fkey FOREIGN KEY (idutilisateur) REFERENCES utilisateur(id);