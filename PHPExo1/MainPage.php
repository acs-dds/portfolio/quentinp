<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Portfolio Quentin Papp</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    
    
    <!-- Custom CSS -->
    <link href="css/1-col-portfolio.css" rel="stylesheet">
    <link href="font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://yassinl.dijon.codeur.online/Accueil/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- Page Content -->
    <div class="container">
    
    <?php include('../../yassinl/Accueil/menu.php'); ?>

        <a href="http://quentinp.dijon.codeur.online/PHPExo1/message.php"" target="_blank"><input type="button" value="Message"></a> 
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Portfolio
                    <small>Quentin Papp</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Project One -->
        <div class="row">
            <div class="col-md-7">
                <a href="#">
                    <img class="img-responsive" src="img/img1.png" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h3>Epilpthic Generator</h3>
                <h4>9/11/2016</h4>
                <p>L'épilepsie est une affection chronique se caractérisant par la survenue de convulsions qui sont le résultat de décharges électriques paroxystiques.</p>
                <p>Cette application permet d'en générer une ! :)</p>
                <a class="btn btn-primary" href="EpilpthicGenerator.php">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Project Two -->
        <div class="row">
            <div class="col-md-7">
                <a href="#">
                    <img class="img-responsive" src="img/img2.jpg" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h3>Simon's Game</h3>
                <h4>2/12/2016</h4>
                <p>Le Simon trouve son origine dans le jeu pour enfant Jacques a dit. Le jeu, électronique, éclaire une des quatre couleurs et produit un son toujours associé à cette couleur.</p>
                <p>Le joueur doit alors appuyer sur la touche de la couleur qui vient de s'allumer dans un délai assez court. Le jeu répète la même couleur et le même son, puis ajoute au hasard une nouvelle couleur.</p>
                <a class="btn btn-primary" href="simon.php">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Project Three -->
        <div class="row">
            <div class="col-md-7" style="background-color: grey;">
                <a href="#">
                    <i class="fa fa-cog fa-spin" style="font-size: 250px; color: white; margin-left: 35%;"></i>
                </a>
            </div>
            <div class="col-md-5" style="text-align: center">
                <h3>IN PROGRESS</h3>
                <h4></h4>
                <p></p>
                <a class="btn btn-primary" href="#" disabled="disabled">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Project Four -->
        <div class="row">

            <div class="col-md-7" style="background-color: grey;">
                <a href="#">
                    <i class="fa fa-cog fa-spin" style="font-size: 250px; color: white; margin-left: 35%;"></i>
                </a>
            </div>
            <div class="col-md-5" style="text-align: center">
                <h3>IN PROGRESS</h3>
                <h4></h4>
                <p></p>
                <a class="btn btn-primary" href="#" disabled="disabled">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Project Five -->
        <div class="row">
            <<div class="col-md-7" style="background-color: grey;">
                <a href="#">
                    <i class="fa fa-cog fa-spin" style="font-size: 250px; color: white; margin-left: 35%;"></i>
                </a>
            </div>
            <div class="col-md-5" style="text-align: center">
                <h3>IN PROGRESS</h3>
                <h4></h4>
                <p></p>
                <a class="btn btn-primary" href="#" disabled="disabled">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->
    
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>