<?php

class Xdb {
	private static $name = '';
	private static $got = false;

	private static function init() {
		if (strlen(self::$name) == 0)
			self::$name = explode('.', $_SERVER['SERVER_NAME'])[0];
	}

	public static function __callStatic($func, $args) {
		self::init();
		return call_user_func_array([get_class(),"_$func"], $args);
	}

	private static function _get() {
		if (self::$got) trigger_error('Modifications non sauvegardées depuis le précédent get()');
		self::$got = true;
		if (file_exists($file = __DIR__."/xdb/".self::$name.".json")) {
			return json_decode(file_get_contents($file), true);
		} else {
			touch($file);
			return [];
		}
	}

	private static function _reset() {
		unlink(__DIR__."/xdb/".self::$name.".json");
	}

	private static function _set($data) {
		if (!is_array($data)) $data = [$data];
		if (!file_exists($file = __DIR__."/xdb/".self::$name.".json")) touch($file);
		file_put_contents($file, json_encode($data));
		self::$got = false;
	}
}