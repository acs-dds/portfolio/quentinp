<?php session_start(); ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Contact</title>
		<style type="text/css">
			form {
			    /* Pour le centrer dans la page */
			    margin: 0 auto;
			    width: 500px;
			    /* Pour voir les limites du formulaire */
			    padding: 1em;
			    border: 1px solid #CCC;
			    border-radius: 1em;
			}

			label {
			    /* Afin de s'assurer que toutes les étiquettes aient la même dimension et soient alignées correctement */
			    display: inline-block;
			    width: 90px;
			    text-align: right;
			}

			.a {
				margin-bottom: 35px;
			}

			.b {
				margin-right: 75px;
				float: left;
			}

			textarea {
				width: 500px; 
				height: 150px;
			}

			.button {
    			padding: 5px;
    			margin: 5px;
    			margin-left: 34%;
			}
		</style>
	</head>
	<body>
		<form method="post" action="info.php">
		    <div class="b">
				<div>
			        <label for="nom">Nom :</label>
			        <input type="text" id="nom" name="nom" placeholder="Ex : Flantier" />
			    </div>
			    <div class="a">
			        <label for="prenom">Prenom :</label>
			        <input type="text" id="prenom" name="prenom" placeholder="Ex : Noël" />
			    </div>
		    </div>
		    <div>
		    	<select name="menu" size="3">
					<option name="des1" id="Yassine">Yassine</option>
					<option name="des2" id="Quentin">Quentin</option>
					<option name="des3" id="Clement">Clement</option>
				</select>
		    </div>
		    <div>
		        <textarea id="message" name="message" >Ce message est asser long pour le test.</textarea>
		    </div>
		    <div class="button">
        		<button type="submit" id="btn">Envoyer votre message</button>
    		</div>
		</form>

			<?php if (isset($_SESSION['erreurs'])) { ?>
			<ul>
				<?php foreach ($_SESSION['erreurs'] as $erreur) { ?>
				<li><?php echo $erreur; ?></li>
				<?php }
				unset($_SESSION['erreurs']); ?>
			</ul>
			<?php } ?>	
	</body>
</html>