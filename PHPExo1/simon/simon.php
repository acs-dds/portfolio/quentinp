<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<title>Simon</title>
	<link rel="stylesheet" type="text/css" href="simon/style.css">
	<link rel="stylesheet" type="text/css" href="simon/med.css">
	<link rel="stylesheet" type="text/css" href="simon/dif.css">

</head>

<body class="bod">

	<h1 class="t_simon">Jeu du Simon!</h1>

	<section>
		<input class="niveau" type="button" value="Mode Facile" id="ButEasy">
		<input class="niveau2" type="button" value="Mode Medium" id="ButMed">
		<input class="niveau3" type="button" value="Mode Difficile" id="ButHard">
	</section>	

	<div class="simon" id="a" style="margin-left: 450px;">

		<input class="s1" id="b1" type="button" name="">
		<input class="s2" id="b2" type="button" name="">
		<section class="simon2"><input class="s3" id="b3" type="button" name="">
		<input class="s4" id="b4" type="button" name=""></section>

	</div>
	<div id="b">
		<input class="s5" id="b1" type="button" name="">
		<input class="s6" id="b2" type="button" name="">
		<input class="s7" id="b3" type="button" name="">
		<input class="s8" id="b4" type="button" name="">
		<input class="s9" id="b5" type="button" name="">
		<input class="s10" id="b6" type="button" name="">
	</div>

	<div id="c">
		<input class="s11" id="b1" type="button" name="">
		<input class="s12" id="b2" type="button" name="">
		<input class="s13" id="b3" type="button" name="">
		<input class="s14" id="b4" type="button" name="">
		<input class="s15" id="b5" type="button" name="">
		<input class="s16" id="b6" type="button" name="">
		<input class="s17" id="b7" type="button" name="">
		<input class="s18" id="b8" type="button" name="">
		<input class="s19" id="b9" type="button" name="">
	</div>

	<script type="text/javascript">

        document.getElementById("ButEasy").addEventListener("click",playEasy);
        document.getElementById("ButMed").addEventListener("click",playMed);
        document.getElementById("ButHard").addEventListener("click",playHard);

        document.getElementById('a').style.visibility = 'visible'; 
	    document.getElementById('b').style.visibility = 'hidden'; 
	    document.getElementById('c').style.visibility = 'hidden';

        function playEasy () { 
			document.getElementById('a').style.visibility = 'visible'; 
	        document.getElementById('b').style.visibility = 'hidden'; 
	        document.getElementById('c').style.visibility = 'hidden';
        }

        function playMed () { 
			document.getElementById('a').style.visibility = 'hidden'; 
	        document.getElementById('b').style.visibility = 'visible';
	        document.getElementById('c').style.visibility = 'hidden'; 
        }
        function playHard () { 
			document.getElementById('a').style.visibility = 'hidden'; 
	        document.getElementById('b').style.visibility = 'hidden';
	        document.getElementById('c').style.visibility = 'visible'; 
        }

        var sequence = [];
        var joueur = [];
        var boutons = [document.getElementById("b1"),
        			  document.getElementById("b2"),
        			  document.getElementById("b3"),
        			  document.getElementById("b4")];

		document.getElementById("ButEasy").addEventListener("click", start);
		function start () {
				for (var i = 0; i < 4; i++) {
				sequence.push(boutons[Math.floor(Math.random() * 4)]);
				}

				var courant = 0;

				function allumer() {
					function eteindre(bouton_a_eteindre) {
						bouton_a_eteindre.classList.remove('actif');
					}
					// allumer le bouton
					sequence[courant].classList.add('actif');
					// différer l'extinction de 600ms
					setTimeout(eteindre, 600, sequence[courant]);
					// différer l'allumage du bouton suivant dans 700ms
					courant++;
					if (courant < sequence.length) {
						setTimeout(allumer, 700);
					}
				}
			
			function play(number){
				joueur.push(document.getElementById("b" + number));

				if(joueur[joueur.length - 1] !== sequence[joueur.length - 1]){
					courant = 0;
					joueur = [];
					sequence = [];
					alert("PERDU");
				}
				else{
					if(joueur.length == sequence.length){
						courant = 0;
						joueur = [];
						sequence.push(boutons[Math.floor(Math.random() * 4)]);
						setTimeout(allumer, 700);
					}
				}
			}

			document.getElementById("b1").addEventListener("click",function(){play(1)});
			document.getElementById("b2").addEventListener("click",function(){play(2)});
			document.getElementById("b3").addEventListener("click",function(){play(3)});
			document.getElementById("b4").addEventListener("click",function(){play(4)});

			allumer();
		}

	</script>
</body>
</html>