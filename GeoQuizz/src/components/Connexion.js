import React from "react";
import Header from "./Header";

export default class Connexion extends React.Component {

	constructor(props) {
		super(props);

		this.state = {'username': ""};
		
		this.getName = this.getName.bind(this);

	}// fin de constructor

	getName(){
//on souhaite envoyer le 'username' à /connexion à l'aide d'un formulaire en méthode post
		var form = new FormData();
		form.append('username', this.state.value);

		fetch('http://faridl.dijon.codeur.online:11001/connexion', {
          method: 'POST',
          /*credentials: 'include',*/
          headers: {
            'Origin' : 'http://quentinp.dijon.codeur.online:3002/',
          },
          body: form
      }) // fin de fetch
      // .then((res) => {
      //   console.log("ok");
      //   console.log(res.text());
      //   return res.text();
      // })
      .then((res) => {
        this.setState({username: res});
      })
			
	} // fin de sendName()

	

	render() {
		return (
				<section>
					<form>
						<label>
						  name:
						  <input 
						  	name="username" 
						  	type="text" 
						  	value={this.state.username}
						  	onChange = {this.getName}
						  	 />
						  	}
						</label>
						<input type="submit" value="Submit" onClick={this.Layout} />
					</form>
				</section>
		);
	}
} 