<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'quentinp');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'quentinp');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'du3M8T3c');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@a$Q-kQ;593hl z&mOSs_;A )}/uc!BW9!s1Ni:0Yiu$]2yR@_`%SYB.4#B*7I &');
define('SECURE_AUTH_KEY',  '2+xH_6l<X;OcAbtmLc}R}^E(]D{lV:K7J8F:*mCk+y#B<tl6tniM*:^{ncqxSJ4I');
define('LOGGED_IN_KEY',    'V(|B/YeHkGEO7gD?a}fWXpl60) &7VF[n]F&3,_V5wZt&I,]ObE0ln}o&uT0d4/[');
define('NONCE_KEY',        '4uwdE_5PLj+Hp^a]&4T6 @&NX308+.NzL2C.Pi^0Afi|.R?MC)8|]d(Vx&cJSY;l');
define('AUTH_SALT',        '{SO/!%3)P2?/Z3d&CPsYl{|V[GyXA?R1sBx@HK=9h|H?Eheb-pky6XM>kzO5OOH{');
define('SECURE_AUTH_SALT', '64b` a,z8. Qngyhlw)5;DXoDUZ[ra1JVV643K-Amj;=sHA.P!{w.bMnnV(0hRVL');
define('LOGGED_IN_SALT',   'ATPE!cjaD,;6MOhY/gDIhLsa>#@*uOFpxc4KK^WBd7aJE8V<Kbcf?oR@F29b~b|p');
define('NONCE_SALT',       'Y~r W o(C=BzN3,t~q`/SOD=Ip>hg[J>8NZnq+ZNI>ps@m ,XkW.MF@L0*t6&#LZ');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'brEeDInGdick5';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');