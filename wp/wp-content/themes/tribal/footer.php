<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package tribal
 */
?>

	</div><!-- #content -->
	
</div><!-- #page -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript">
		$( document ).ready(function() {
			// Lance la fonction au click d'un article
	    	$("article").click(function (){ 
	    		// Affectation de la class son (class de l'audio dans content-tribal_4_col.php)
	    		var sound = document.getElementsByClassName("son");  
	    		// Parcours tous les sons 
	    		for (i = 0; i < sound.length; i++)
	    		{
	    			sound[i].pause();  // Met en pause le son
	    			sound[i].currentTime = 0; //reset le son au debut
	    		}
	    		// Création d'une variable qui a comme contenue un chiffre associé a l'ID de bouton clické (par exemple quand je click sur l'article post-60, idbtn sera = à 60)
	    		var idbtn = $(this).attr("id").substring(5); 
	    		// Lancement du son associé à l'article clické
	    		$('#'+idbtn)[0].play();
    		});
	    });
	</script>

<?php wp_footer(); ?>

</body>
</html>