<?php

class Client {
	private $nom;
	private $prenom;
	private $login;
	private $typologie;
	private $voie;
	private $codePostal;
	private $ville;

	public function __construct($array) {
		$this->prenom = $array[0];
		$this->nom = $array[1];
		$this->login = $array[2];
		$this->typologie = $array[4];
		$this->voie = $array[5];
		$this->codePostal = $array[6];
		$this->ville = $array[7];

	}

	public function getTypologie() {
		return $this->typologie;
	}

	public function getNom() {
		return $this->nom;
	}

	public function getPrenom() {
		return $this->prenom;
	}

	public function getVoie() {
		return $this->voie;
	}

	public function getCodePostal() {
		return $this->codePostal;
	}

	public function getVille() {
		return $this->ville;
	}
}
?>

 

