<?php require_once 'classes/mappeur.php'; 
session_start();?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

	<?php $obj = Mapper::getProduit($_POST["ref"]);
	$_SESSION["objet"] = $obj;
	$epaisseur = $_POST['epaisseur'];
	$_SESSION["epaisseur"] = $epaisseur;
	$varLong = $obj->getLongueur() /5;
	$varLarg = $obj->getLargeur() / 5;
	$varLongI = $obj->getLongueur();
	$varLargI = $obj->getLargeur();

	if ($varLarg == $varLong) {
		$varLarg = 500;
		$varLong = 500;
		$margLeft = "33%";		
	}
	if ($varLarg != $varLong) {
		$varLarg = 500;												
		$varLong = 750;
		$margLeft = "26%";
	}?>

	<style>
		.longueur {
		height: 20px; 
		width: auto;  
		margin-top: -7%; 
		margin-left: <?php echo $margLeft ?>;
		transform: rotate(-90deg);
		transform-origin: left top 0;
	}
		rect {
		opacity: 0.4;
		fill : white;
	}
	</style>
</head>
<body class="body_catalogue">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/svg.js/2.3.6/svg.js"></script>
	<script src="svg.draw.js"></script>
	<script src="raphael.min.js"></script>
	<article class="global">
		<center><div class="largeur">
			<h2 style="font-family: 'Lato', sans-serif;"><?php echo "Largeur : ".$varLargI." mm"; ?></h2>
		</div></center>
	 		<center><svg id="canvas_container" width="<?php echo $varLong ?>" height="<?php echo $varLarg ?>" style=" background-color: #d9b38c;">
	 		</svg></center>
	 	<div class="longueur">
	 		<h2 style="font-family: 'Lato', sans-serif"><?php echo "Longueur : ".$varLongI." mm"; ?></h2>
		</div>
	</article>
	<div style="margin-left: 46%">
	<form id="recap" method="post" action="pageRecap.php">
		<button class="bouton" type="submit" id="valid">Valider</button>
	</form>
		<button class="bouton" type="button" id="Reset">Effacer</button>
	</div>
	<script>
		$(document).ready(function () {
		var draw = new SVG('canvas_container').size(<?php echo $varLong ?>, <?php echo $varLarg ?>);
		draw.rect().draw();
			$("#Reset").on("click", function reset () {
				var idRec = $('rect').attr('id');
				document.getElementById(idRec).remove();
				draw.rect().draw();
			});
			$("#valid").on("click", function recupDonne () {
				var trans = $('rect').attr('width');
				var transP = 100*trans/<?php echo $varLong ?>;
				var widthRec =  transP * <?php echo $varLongI ?> /100;
				$("#recap").append("<input type=\"hidden\" name=\"hRec\" value=\""+widthRec+"\"/>");
				var trans2 = $('rect').attr('height');
				var transP2 = 100*trans2/500;
				var heightRec =  transP2 * <?php echo $varLargI ?> /100;
				$("#recap").append("<input type=\"hidden\" name=\"wRec\" value=\""+heightRec+"\"/>");
				var xRec = $('rect').attr('x');
				$("#recap").append("<input type=\"hidden\" name=\"xRec\" value=\""+xRec+"\"/>");
				var yRec = $('rect').attr('y');
				$("#recap").append("<input type=\"hidden\" name=\"yRec\" value=\""+yRec+"\"/>");
			});
		});	
	</script>
	<footer>
		<center><div class="informations_footer">
			<p>NODEX - 14, rue du Général de Gaulle - Z.I. Le Pré vert - 39500 Moirans-en-Montagne<br>
			Tél: 03.80.45.78.66<br>
			Copyright © NODEX-Industrie</p>
		</div></center>
	</footer>
</body>
</html>