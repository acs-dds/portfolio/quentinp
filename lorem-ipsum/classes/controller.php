<?php 
require_once "classes/mapper.php";

class Controller {
	private $mapper;
	
	public function __construct() {
		$this->mapper = new Mapper();
	}

	public function actionGenerateAllParagraphe () {
		if (isset($_GET['Mot']) && isset($_GET['Paragraphe']) && isset($_GET['theme'])) {
			if ($_GET['Mot'] > 500 || $_GET['Paragraphe'] > 25 || $_GET['Mot'] < 1 || $_GET['Paragraphe'] < 1) {
				?><script>alert("Nombre de mot ou de paragraphe incohérent")</script><?php
				return $this->mapper->generateAllParagraphe();
			}else
			return $this->mapper->generateAllParagraphe($_GET['Paragraphe'],$_GET['Mot'],$_GET['theme']);
		}else
			return $this->mapper->generateAllParagraphe();		
	}
}