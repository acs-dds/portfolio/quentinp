<?php
require_once "classes/controller.php";
$c = new controller();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>index</title>

		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Ubuntu|Poiret+One">
		<link rel="stylesheet" type="text/css"	href="http://quentinp.dijon.codeur.online/PHPExo1/txt.wav-master/dist/css/txt.wav.min.css"/>
	</head>
	<body>
		<h1 class="txtwav bounce">
			<span class="og">L</span>
			<span>o</span>
			<span>r</span>
			<span>e</span>
			<span>m</span>
			<span> </span>
			<span>I</span>
			<span>p</span>
			<span>s</span>
			<span>u</span>
			<span>m</span>
			<span> </span>
			<span>G</span>
			<span>e</span>
			<span>n</span>
			<span>e</span>
			<span>r</span>
			<span>a</span>
			<span>t</span>
			<span>o</span>
			<span>r</span>
		</h1>
		<form>
			<input class="txt" type="number" name="Mot" placeholder="Nombre de mots">.
			<input class="txt" type="number" name="Paragraphe" placeholder="Nombre de paragraphes">
			<div class="theme">
				<input type="radio" name="theme" value="dataDefault" checked="checked">
				<label for="Default">Default</label>
				<input type="radio" name="theme" value="dataZombie">
				<label for="Zombie">Zombie Ipsum</label>
				<input type="radio" name="theme" value="dataCupcake">
				<label for="Cupcake">Cupcake Ipsum</label>
				<br>
				<input class="btnValideid" type="submit" value="Générer">
			</div>	
		</form>
		<div class="text">
			<?php echo $c->actionGenerateAllParagraphe();?>
		</div>
	</body>
</html>